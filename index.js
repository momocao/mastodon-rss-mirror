import Parser from 'rss-parser';
import fetch from 'node-fetch';
import commandLineArgs from 'command-line-args';
import commandLineUsage from 'command-line-usage';
import * as dotenv from 'dotenv';
dotenv.config();

import { options } from './options.js';

const parser = new Parser();
const args = commandLineArgs(options);

if (args.help) {
  const usage = commandLineUsage([
    {
      header: 'Mastodon RSS mirror',
      content: `This little tool lets you post the latest posts of a RSS feed to Mastodon, useful to program posts based on new articles published in your blog. The program will parse your RSS feed and post to Mastodon if it finds a new post newer than the last.
        
      The ideal way to run this is with a cronjob, which should be adjusted depending on your post frequency.`
    },
    {
      header: 'Usage',
      content: 'node index.js --feed https://blog.joinmastodon.org/index.xml --account https://mastodon.social/@Mastodon'
    },
    {
      header: 'Options',
      optionList: options,
    },
    {
      header: 'Code',
      content: 'Send patches and suggestions to {underline https://invent.kde.org/momocao/mastodon-rss-mirror}.'
    }
  ]);
  console.log(usage);
  process.exit();
};

const fetchFeed = async (url) => {
  const feed = await parser.parseURL(url);
  if (!feed) {
    throw new Error('Invalid feed, please pass a correct RSS or Atom feed, e.g. https://blog.joinmastodon.org/index.xml');
  }

  return feed.items;
};

// return date of the first post, if there are no posts, return null
const fetchLastPostDate = async(posts) => {
  if (posts.length > 0) {
    return +new Date(posts[0].isoDate);
  }
  return null;
}

// get list of posts to publish to mastodon
const getPostsToPublish = async(feed, posts) => {
  // if there's no posts on mastodon return the first article in the feed
  if (!posts || posts.length === 0) {
    return feed.slice(0, 1);
  }

  // use order in the feed
  if (args.order) {
    const idx = feed.findIndex((d) => posts[0].contentSnippet.includes(getCleanLink(d.link)));
    if (idx === -1) {
      return [];
    }
    return feed.slice(0, idx);
  }

  // use date comparison
  const lastPostDate = fetchLastPostDate(posts);
  const filteredPosts = feed.filter((d) => {
    return +new Date(d.isoDate) > lastPostDate;
  });

  return filteredPosts;
}

const getCleanLink = (link) => {
  return link.replace('?utm_source=atom_feed', '');
}

const sleep = ms => {
  return new Promise(resolve => setTimeout(resolve, ms))
}

// if there is more than one new post, wait 1m to publish each
const publishPosts = async(posts) => {
  if (!posts || posts.length === 0) {
    console.log('No new posts to publish :(');
    process.exit();
  }

  // if there's more than one post we should publish them in chronological order
  const sortedPosts = posts.slice().reverse();
  console.log(`Found ${sortedPosts.length} new post${sortedPosts.length > 1 ? 's' : ''} to publish!`);

  for (let i = 0; i < sortedPosts.length; i++) {
    const post = sortedPosts[i];
    await publishPost(post);

    // wait a bit if there's more than one post and we are not in the last one yet
    if (posts.length > 1 && i + 1 < sortedPosts.length) {
      await sleep(60 * 1e3);
    }
  }
}

const publishPost = async(post) => {
  // find server url by splitting the @ separator
  // this could be flaky, revisit if troublesome
  const serverUrl = args.account.split('@')[0];
  if (!serverUrl) {
    throw new Error('Invalid Mastodon URL, please use the following format, e.g. https://mastodon.social/@Mastodon');
  }

  // remove feed query string
  const cleanLink = getCleanLink(post.link);

  // create data to pass to Mastodon's API
  const params = new URLSearchParams();
  params.append('status', `${post.title} ${cleanLink}`);
  params.append('language', 'en');

  // send request
  // https://docs.joinmastodon.org/methods/statuses/#create
  const res = await fetch(`${serverUrl}api/v1/statuses`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${process.env.ACCESS_TOKEN}`,
      // avoids a duplicate post with the same status for 1h
      // https://docs.joinmastodon.org/methods/statuses/#headers
      // 'Idempotency-Key': post.id,
    },
    body: params,
  });
  
  const json = await res.json();
  console.log('Post published!', json.url);
}

const main = async() => {
  if (!args.feed) {
    throw new Error('Please pass the URL of the RSS feed you\'re fetching from, e.g.:\nnode index.js --feed https://blog.joinmastodon.org/index.xml --account https://mastodon.social/@Mastodon');
  }
  if (!args.account) {
    throw new Error('Please pass the URL of the Mastodon account you want to post to, e.g.:\nnode index.js --account https://mastodon.social/@Mastodon --feed https://blog.joinmastodon.org/index.xml');
  }

  // get all the posts in our feed
  const feed = await fetchFeed(args.feed);
  if (feed.length === 0) {
    throw new Error('No entries found in this RSS feed, please make sure there are posts published.');
  }

  // get the mastodon posts that we already published
  const posts = await fetchFeed(`${args.account}.rss`);

  // if we have new posts, find the ones to publish
  // if we don't have posts, return first RSS entry
  const postsToPublish = await getPostsToPublish(feed, posts);

  // post to mastodon
  await publishPosts(postsToPublish);
}

main();
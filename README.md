# Mastodon RSS mirror

Posts an update to Mastodon if there's a new post on a RSS feed. Useful to automate links with new articles from your blog. 

The best way to run this is with a cronjob scheduled according to your publish frequency (once a day if there's posts every few days, every hour if there's multiple posts a day).

This project powers the [Planet KDE Mastodon bot](https://mas.to/@planetkde).

## Usage

First, create a Mastodon app by going to the development settings. Select the `write:statuses` permission and copy the access token in a secure place.

Now, clone the repository and install the dependencies (use node v16).

```bash
npm install
```

To add your credentials, copy `.env.example` and create a file called `.env`. Paste your access token inside.

To run the app run `index.js` with the RSS feed you're fetching and the Mastodon URL of the account you want to post to:

```bash
node index.js --feed https://blog.joinmastodon.org/index.xml --account https://mastodon.social/@Mastodon
```

The script will post an update to the Mastodon account if there are posts with a newer date than the last published post. If there are no posts it will post the first item on the feed.

To print more usage information:

```bash
node index.js --help
```
export let options = [
  {
    name: 'account',
    alias: 'a',
    type: String,
    description: 'URL of the Mastodon account you want to post to, e.g. https://mas.to/@planetkde.'
   },
  {
    name: 'feed',
    alias: 'f',
    type: String,
    description: 'RSS or Atom feed URL you want to fetch posts from, e.g. https://planet.kde.org/atom.xml.'
  },
  {
    name: 'date',
    alias: 'd',
    type: Boolean,
    description: 'Compares article dates in the RSS feed to find new posts to publish. This is useful for most blogs. It\'s the default.'
  },
   {
    name: 'order',
    alias: 'o',
    type: Boolean,
    description: 'Compares the order of the articles in the RSS feed to find new posts to publish. This is a better for blogs where posts get backdated or are published with the same date.'
  },
  {
    name: 'help',
    alias: 'h',
    type: Boolean,
    description: 'Display this help prompt.'
  },
];